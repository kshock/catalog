import unittest
from unittest.mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService


class CatalogTest(unittest.TestCase):

    def test_should_get_person_by_pesel(self):
        # given
        p = Person("imie", "nazwisko", "pesel")
        mock = DB()  # stworzenie obiektu ktory bedzie mockiem, zero magii
        mock.get_person = MagicMock(return_value=p)  # szykujemy zachowanie jednej metody, to jest mock
        c = Catalog(mock, None)  # tworzymy testowany obiekt, spelniajac zaleznosc mockiem

        # when
        result = c.get_person("pesel")  # wlasciwy test

        # then
        self.assertEqual(p, result)  # weryfikacja po tescie
        mock.get_person.assert_called_with("pesel")  # dodatkowo weryfikujemy obiekt zastepczy

        # Mozna tez tak, zdefiniowac liste wolan i potem weryfikowac cala liste:
        # calls = [call("pesel")];
        # mock.get_person.assert_has_calls(calls, any_order=False)

    def test_should_get_first_pesel(self):
        # given
        first_name = "Robcio"
        last_name = "Maslo"
        pesel = ["4124124"]
        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=pesel)
        c = Catalog(mock, None)

        # when
        result = c.get_pesel(first_name, last_name)

        # then
        self.assertEqual(pesel[0], result)
        mock.get_pesel_list.assert_called_with("Robcio", "Maslo")

    def test_should_get_list_of_pesels(self):
        # given
        first_name = "Robcio"
        last_name = "Maslo"
        pesels = [4124124, 1, 7537]
        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=pesels)
        c = Catalog(mock, None)

        # when
        result = c.get_pesel_list(first_name, last_name)

        # then
        self.assertEqual(pesels, result)
        mock.get_pesel_list.assert_called_with("Robcio", "Maslo")

    def test_should_get_add_person_with_right_pesel(self):
        # given
        name = "Jakub"
        surname = "Obligacja"
        pesel = 68041374123
        mock = DB()
        inspection_mock = PeselService()
        inspection_mock.verify = MagicMock(return_value=True)
        c = Catalog(mock, inspection_mock)

        # when
        c.add_person(name, surname, pesel)

        # then
        self.assertTrue(inspection_mock.verify)

    def test_should_get_add_person_with_very_bad_pesel(self):
        # given
        name = "Jakub"
        surname = "Obligacja"
        pesel = 68041374123
        mock = DB()
        p = Person(name, surname, pesel)
        inspection_mock = PeselService()
        inspection_mock.verify = MagicMock(return_value=False)
        c = Catalog(mock, inspection_mock)

        # when
        result = inspection_mock.verify(pesel)
        try:
            c.add_person(name, surname, pesel)
        except:
            mock.add_person(p)

        # then
        self.assertFalse(result)

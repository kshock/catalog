package pl.poznan.put.spio.db;

import pl.poznan.put.spio.service.Person;

import java.util.List;

public interface DB {

    List<String> getPESELList(String name, String surname);

    Person getPerson(String pesel);

    void insertPerson(Person person);

}
